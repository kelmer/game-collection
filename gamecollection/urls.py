from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.views.generic import RedirectView

urlpatterns = [
                  # Examples:
                  # url(r'^$', 'gamecollection.views.home', name='home'),
                  # url(r'^blog/', include('blog.urls')),

                  url(r'^admin/', include(admin.site.urls)),
                  url(r'', include('collection.urls')),
                  url(r'^collection/', include('collection.urls')),
                  url(r'^imagefit/', include('imagefit.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
