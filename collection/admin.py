from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Company
from .models import Game
from .models import Goodie
from .models import Image

admin.site.register(Company)
admin.site.register(Game)
admin.site.register(Goodie)
admin.site.register(Image)