# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0005_auto_20170422_1251'),
    ]

    operations = [
        migrations.AddField(
            model_name='goodie',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='goodie',
            name='have',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='image',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='game',
            name='have',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='game',
            name='spanish',
            field=models.BooleanField(default=True),
        ),
    ]
