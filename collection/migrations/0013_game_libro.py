# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0012_game_hide'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='libro',
            field=models.BooleanField(default=True),
        ),
    ]
