# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0014_auto_20170711_1945'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='platform',
            field=models.TextField(default='PC'),
        ),
    ]
