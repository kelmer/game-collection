# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0003_company_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='cover',
            field=models.ImageField(blank=True, upload_to='covers'),
        ),
        migrations.AlterField(
            model_name='image',
            name='url',
            field=models.ImageField(blank=True, upload_to='photos'),
        ),
    ]
