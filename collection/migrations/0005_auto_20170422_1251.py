# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0004_auto_20170422_1248'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='have',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='game',
            name='spanish',
            field=models.BooleanField(default=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='game',
            name='year',
            field=models.IntegerField(default=1995),
            preserve_default=False,
        ),
    ]
