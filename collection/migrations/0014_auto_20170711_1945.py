# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0013_game_libro'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='price',
            field=models.FloatField(default=0.0),
        ),
        migrations.AlterField(
            model_name='game',
            name='libro',
            field=models.BooleanField(default=False),
        ),
    ]
