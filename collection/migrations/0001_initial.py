# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=500)),
                ('logo', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=500)),
                ('cover', models.CharField(max_length=250)),
                ('edition', models.CharField(max_length=500)),
                ('developer', models.ForeignKey(related_name='game_dev', to='collection.Company')),
                ('distributor', models.ForeignKey(related_name='game_dist', to='collection.Company')),
                ('publisher', models.ForeignKey(related_name='game_pub', to='collection.Company')),
            ],
        ),
        migrations.CreateModel(
            name='Goodie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=500)),
                ('game', models.ForeignKey(to='collection.Game')),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('url', models.CharField(max_length=250)),
                ('game', models.ForeignKey(to='collection.Game')),
            ],
        ),
    ]
