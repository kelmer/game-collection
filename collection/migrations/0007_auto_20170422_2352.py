# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0006_auto_20170422_1251'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='company',
            options={'ordering': ('name',)},
        ),
        migrations.AlterModelOptions(
            name='game',
            options={'ordering': ('name',)},
        ),
        migrations.AddField(
            model_name='game',
            name='estado',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='game',
            name='otros',
            field=models.BooleanField(default=False),
        ),
    ]
