# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0007_auto_20170422_2352'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='origen',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AlterField(
            model_name='game',
            name='edition',
            field=models.CharField(max_length=500, blank=True),
        ),
    ]
