# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0011_game_box'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='hide',
            field=models.BooleanField(default=False),
        ),
    ]
