from django.db import models

# Create your models here.

from django.db import models
from django.utils import timezone

class Game(models.Model):
    name = models.CharField(max_length=500)
    year = models.IntegerField()
    cover = models.ImageField(upload_to='covers', blank=True)
    edition = models.CharField(max_length=500, blank=True)
    have = models.BooleanField(default=False)
    developer = models.ForeignKey('Company', related_name='game_dev')
    publisher = models.ForeignKey('Company', related_name='game_pub')
    distributor = models.ForeignKey('Company', related_name='game_dist')
    spanish = models.BooleanField(default=True)
    estado = models.TextField(blank=True)
    otros = models.BooleanField(default=False)
    origen = models.CharField(max_length=500, blank=True)
    description = models.TextField(blank=True)
    orden = models.IntegerField(default=0)
    box = models.BooleanField(default=False)
    libro = models.BooleanField(default=False)
    hide = models.BooleanField(default=False)
    price = models.FloatField(default=0.0)
    platform = models.TextField(default="PC")
    class Meta:
        ordering = ('name',)
    def __str__(self):
        return "{name} ({edicion})".format(name=self.name, edicion=self.edition)


class Company(models.Model):
    name = models.CharField(max_length=500)
    logo = models.ImageField(upload_to='logos', blank=True)

    class Meta:
        ordering = ('name',)
    def __str__(self):
        return self.name

class Goodie(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField(blank=True)
    have = models.BooleanField(default=True)
    game = models.ForeignKey('Game')

    def __str__(self):
        return "{name} - {game}".format(name=self.name, game=self.game)


class Image(models.Model):
    url = models.ImageField(upload_to='photos', blank=True)
    description = models.TextField(blank=True)
    game = models.ForeignKey('Game')

    def __str__(self):
        return "{description} - {game}".format(description=self.description, game=self.game)

