from django import forms
from django.forms import HiddenInput

from .models import Game
from .models import Goodie
from .models import Image


class GoodieForm(forms.ModelForm):
    class Meta:
        model = Goodie
        fields = ('name', 'have', 'game')
        widgets = {'game': forms.HiddenInput()}

class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = { 'url'}


class ContactForm(forms.Form):
    contact_name = forms.CharField(label='Nombre')
    contact_email = forms.EmailField(label='Email', required=True)
    content = forms.CharField(label='Contenido',
        required=True,
        widget=forms.Textarea
    )