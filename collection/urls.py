
from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.have_list),
    url(r'^have/$', views.have_list),
    url(r'^want/$', views.want_list),
    url(r'^keepcase/$', views.keepcase),
    url(r'^libros/$', views.libros),
    url(r'^missing/$', views.missing),
    url(r'^otros/$', views.otros),
    url(r'^contacto/$', views.contacto),
    url(r'^about/$', views.about),
    url(r'^test/$', views.contacto),
    url(r'^all/$', views.all),
    url(r'^game/(?P<pk>[0-9]+)/$', views.game_detail),

    url(r'^upload_image/(?P<pk>[0-9]+)/$', views.upload_image),

]
