from django.shortcuts import render
from .models import Game
from .models import Company
from .models import Image
from .models import Goodie
from django.db.models import Q
from .forms import GoodieForm, ImageForm
from django.template import RequestContext as ctx
from django.http import HttpResponse
from collection.forms import ContactForm
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.contrib import messages


# Create your views here.



def have_list(request):
    dev_id = request.GET.get("dev_id", None)
    year = request.GET.get("year", None)
    order = request.GET.get("order", None)

    baseQuery = Game.objects
    baseQuery = baseQuery.filter(have=True, otros=False, box=True, hide=False)

    modelAttribute = {}
    if (dev_id is not None):
        company = Company.objects.get(pk=dev_id)
        baseQuery = baseQuery.filter(Q(developer=company) | Q(publisher=company) | Q(distributor=company))
        modelAttribute.update({'company': company})

    if (year is not None):
        baseQuery = baseQuery.filter(year=year)
        modelAttribute.update({'year': year})

    if (order == 'year'):
       games = baseQuery.order_by('year', 'name')
    else:
       games = baseQuery.order_by('name')
    total = len(games)

    modelAttribute.update({'games_have': games, 'total_have': total})

    if (dev_id is not None or year is not None):
        nHaveQuery = Game.objects
        nHaveQuery = nHaveQuery.filter(have=False, otros=False, box=True, hide=False)
        if (dev_id is not None):
            nHaveQuery = nHaveQuery.filter(Q(developer=company) | Q(publisher=company) | Q(distributor=company))
        if (year is not None):
            nHaveQuery = nHaveQuery.filter(year=year)
        if (order == 'year'):
            games_nhave = nHaveQuery.order_by('year')
        else:
            games_nhave = nHaveQuery.order_by('name')
        total_nhave = len(games_nhave)
        modelAttribute.update({'games_nhave': games_nhave, 'total_nhave': total_nhave})

    return render(request, 'collection/have_list.html', modelAttribute)

def all(request):
    dev_id = request.GET.get("dev_id", None)
    year = request.GET.get("year", None)

    baseQuery = Game.objects
    baseQuery = baseQuery.filter(otros=False, box=True)

    modelAttribute = {}
    if (dev_id is not None):
        company = Company.objects.get(pk=dev_id)
        baseQuery = baseQuery.filter(Q(developer=company) | Q(publisher=company) | Q(distributor=company))
        modelAttribute.update({'company': company})

    if (year is not None):
        baseQuery = baseQuery.filter(year=year)
        modelAttribute.update({'year': year})

    games = baseQuery.order_by('name')
    total = len(games)

    modelAttribute.update({'games_have': games, 'total_have': total})

    return render(request, 'collection/have_list.html', modelAttribute)


def want_list(request):
    spanish = request.GET.get("spanish", False)
    english = request.GET.get("english", False)
    hide = request.GET.get("hide", False)

    games = Game.objects.filter(have=False, otros=False).order_by('-orden', 'name')

    if(spanish):
        games = games.filter(spanish=True)
    elif(english):
        games = games.filter(spanish=False)

    if (not hide):
        games = games.filter(hide=False)
    total = len(games)
    return render(request, 'collection/have_list.html', {'games_nhave': games, 'total_nhave': total})


def otros(request):
    games_have = Game.objects.filter(have=True,otros=True, hide=False, libro=False).order_by('name')
    total_have = len(games_have)
    games_nhave = Game.objects.filter(have=False, otros=True, hide=False, libro=False).order_by('name')
    total_nhave = len(games_nhave)
    return render(request, 'collection/have_list.html', {'games_have': games_have, 'total_have': total_have, 'games_nhave': games_nhave, 'total_nhave': total_nhave})

def libros(request):
    games_have = Game.objects.filter(have=True, otros=True, hide=False, libro=True).order_by('name')
    total_have = len(games_have)
    games_nhave = Game.objects.filter(have=False, otros=True, hide=False, libro=True).order_by('name')
    total_nhave = len(games_nhave)
    return render(request, 'collection/have_list.html', {'games_have': games_have,
                                                         'total_have': total_have,
                                                         'games_nhave': games_nhave,
                                                         'total_nhave': total_nhave,
                                                         'extra_class': 'thin-box'
                                                         })
def missing(request):
    goodies_missing = Goodie.objects.filter(have=False).order_by('game')
    total_missing = len(goodies_missing)

    return render(request, 'collection/goodies_missing.html', {'goodies_missing': goodies_missing,
                                                         'total_missing': total_missing,
                                                         })

def keepcase(request):
    games_have = Game.objects.filter(have=True, otros=False, hide=False, box=False).order_by('name')
    total_have = len(games_have)
    games_nhave = Game.objects.filter(have=False, otros=False, hide=False, box=False).order_by('name')
    total_nhave = len(games_nhave)
    return render(request, 'collection/have_list.html', {'games_have': games_have,
                                                         'total_have': total_have,
                                                         'games_nhave': games_nhave,
                                                         'total_nhave': total_nhave,
                                                         'extra_class': 'thin-box',
                                                         'no_links': True})


def contacto(request):

    form_class = ContactForm
    if request.method == 'POST':
        form = form_class(data=request.POST)
        if form.is_valid():
            contact_name = request.POST.get('contact_name', '')
            contact_email = request.POST.get('contact_email', '')
            form_content = request.POST.get('content', '')

            # Email the profile with the
            # contact information
            template = get_template('contact_template.txt')
        context = Context({
            'contact_name': contact_name,
            'contact_email': contact_email,
            'form_content': form_content,
        })
        content = template.render(context)

        email = EmailMessage(
            "Nuevo contacto desde La Aventura Original",
            content,
            "kelmer@gmail.com",
            ['kelmer@gmail.com'],
            headers={'Reply-To': contact_email}
        )
        email.send()
        messages.success(request, 'El mensaje se ha enviado correctamente.')

        return redirect('collection.views.contacto')

    return render(request, 'collection/contact.html', {
        'form': form_class,
    })




def about(request):
    return render(request, 'collection/about.html')


def game_detail(request, pk):
    game = Game.objects.get(pk=pk)

    if request.method == "POST":
        # goodieForm = GoodieForm(request.POST)
        # if goodieForm.is_valid():
        #     goodie = goodieForm.save(commit=True)
        imageForm = ImageForm(request.POST, request.FILES)

        image = Image(url=request.FILES['file'], game=game)

        if imageForm.is_valid():
            image.save()
            # return HttpResponseRedirect(reverse(request.path))
    else:
        imageForm = ImageForm()

    images = Image.objects.filter(game=game)
    goodies = Goodie.objects.filter(game=game)
    # new_goodie = Goodie()
    # new_goodie.game = game
    # form = GoodieForm(instance=new_goodie)

    # return render(request, 'collection/game.html', {'game': game, 'images': images, 'goodies': goodies, 'form':form})
    return render(request, 'collection/game.html',
                  {'game': game, 'images': images, 'goodies': goodies, 'form': imageForm}, ctx(request))


def upload_image(request, pk):
    game = Game.objects.get(pk=pk)
    if request.method == "POST":
        imageForm = ImageForm(request.POST, request.FILES)

        image = Image(url=request.FILES['file'], game=game)
        image.description = request.GET['description']
        if imageForm.is_valid():
            image.save()
    return HttpResponse('')
