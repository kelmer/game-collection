# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Game Collection manager
Check http://coleccion.gabrielsanmartin.net

### How do I get set up? ###
## Set virtual environment: ##

python3 -m venv myvenv

C:\Users\Name\djangogirls> myvenv\Scripts\activate

~/djangogirls$ source myvenv/bin/activate

manange.py makemigrations
manage.py migrate
manage.py runserver


## Required packages: ##
* appdirs==1.4.3
* Django==1.8
* olefile==0.44
* packaging==16.8
* Pillow==4.1.0
* PyMySQL==0.7.11
* pyparsing==2.2.0
* pytz==2017.2
* six==1.10.0